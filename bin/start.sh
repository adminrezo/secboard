#!/bin/bash

[ -f .env ] || exit 1
source .env
echo "Pulling images"
echo "@@@@@@@@@@@@@@"
docker-compose pull
echo
echo "Building images"
echo "@@@@@@@@@@@@@@@"
docker-compose build 
echo
echo "Post build scripts"
echo "@@@@@@@@@@@@@@@@@@"
sed "s/K2.DOMAIN.NAME/${DOMAIN}/" Docker/web/conf/default.conf.local > Docker/web/conf/default.conf
echo
echo "Running images"
echo "@@@@@@@@@@@@@@"
docker-compose up -d
