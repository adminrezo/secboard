# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get     '/domains/delete',        to: 'domains#destroy_all'
  get     '/domains',               to: 'domains#index'
  post    '/domains',               to: 'domains#create'
  get     '/domains/:id',           to: 'domains#show'
  get     '/domains/:id/update',    to: 'domains#update'
  delete  '/domains/:id',           to: 'domains#destroy'
  get     '/domains/:id',           to: 'domains#show'
  get     '/domains/headers/:id',   to: 'domains#show_headers'
  get     '/domains/tls/:id',       to: 'domains#show_tls'
  get     '/domains/jslibs/:id',    to: 'domains#show_jslibs'
  get     '/domains/cms/:id',       to: 'domains#show_cms'
  get     '/domains/mails/:id',     to: 'domains#show_mail'
  get     '/domains/ports/:id',     to: 'domains#show_ports'
  get     '/domains/subdomains/:id', to: 'domains#show_subdomains'
  get     '/reports/:id', to: 'reports#generate'
  %w[base64 csr dig dnspropagation pass].each do |o|
    get     "/tools/#{o}",          to: "#{o}#index"
    post    "/tools/#{o}",          to: "#{o}#show"
  end
end
