# frozen_string_literal: true

class AddProtocolToPort < ActiveRecord::Migration[6.1]
  def change
    add_column :ports, :protocol, :string
  end
end
