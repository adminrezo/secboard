# frozen_string_literal: true

class DropRoles < ActiveRecord::Migration[6.1]
  def change
    drop_table :roles do |t|
      t.string :name, null: false
      t.timestamps
    end
  end
end
