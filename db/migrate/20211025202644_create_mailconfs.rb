# frozen_string_literal: true

class CreateMailconfs < ActiveRecord::Migration[6.1]
  def change
    create_table :mailconfs do |t|
      t.string :protocol
      t.string :value

      t.timestamps
    end
  end
end
