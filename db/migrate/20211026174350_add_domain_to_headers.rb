# frozen_string_literal: true

class AddDomainToHeaders < ActiveRecord::Migration[6.1]
  def change
    add_column :headers, :domain, :string
    add_reference :headers, :domain, foreign_key: true
  end
end
