# frozen_string_literal: true

class ChangeVersionToStringInJslib < ActiveRecord::Migration[6.1]
  def up
    change_column :jslibs, :version, :string
  end

  def down
    change_column :jslibs, :version, :float
  end
end
