# frozen_string_literal: true

class AddDomainToSubdomains < ActiveRecord::Migration[6.1]
  def change
    add_column :subdomains, :domain, :string
    add_reference :subdomains, :domain, foreign_key: true
  end
end
