# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_02_07_153639) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cms", force: :cascade do |t|
    t.string "name"
    t.string "version"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_cms_on_domain_id"
  end

  create_table "domains", force: :cascade do |t|
    t.string "name"
    t.text "scanresult"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_domains_on_user_id"
  end

  create_table "headers", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_headers_on_domain_id"
  end

  create_table "jslibs", force: :cascade do |t|
    t.string "library"
    t.string "version"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_jslibs_on_domain_id"
  end

  create_table "mailconfs", force: :cascade do |t|
    t.string "protocol"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_mailconfs_on_domain_id"
  end

  create_table "ports", force: :cascade do |t|
    t.integer "number"
    t.string "software"
    t.string "version"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.string "protocol"
    t.index ["domain_id"], name: "index_ports_on_domain_id"
  end

  create_table "subdomains", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_subdomains_on_domain_id"
  end

  create_table "tlsconfigs", force: :cascade do |t|
    t.string "version"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "domain"
    t.bigint "domain_id"
    t.integer "note"
    t.index ["domain_id"], name: "index_tlsconfigs_on_domain_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "cms", "domains"
  add_foreign_key "domains", "users"
  add_foreign_key "headers", "domains"
  add_foreign_key "jslibs", "domains"
  add_foreign_key "mailconfs", "domains"
  add_foreign_key "ports", "domains"
  add_foreign_key "subdomains", "domains"
  add_foreign_key "tlsconfigs", "domains"
end
