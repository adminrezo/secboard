# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should not save user without role' do
    user = User.new
    user.email = 'test@test'
    user.password = 'test@test'
    user.password_confirmation = 'test@test'
    user.confirmed_at = DateTime.now
    assert_not u.save
  end
end
