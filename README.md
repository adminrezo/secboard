# SecBoard

![Pipeline Status](https://gitlab.com/adminrezo/secboard/badges/master/pipeline.svg)

SecBoard leads you to the summit of Web security ⛏ ⛰  !

It gives you a quick security status of your website.

You can use a CLI or a WebUI.

![SecBoard](/public/images/background.webp)

## Requirements

Under Docker :
- docker
- docker-compose

Under Linux (tested on Debian/MySQL) :
- rails 6+
- ruby 3+
- any DB engine but SQlite (Maria, PG, ...) 

To send mails, verify that you are using SPF, DKIM and DMARC protocols.

## With Docker Compose

- Clone the repo `git clone https://gitlab.com/adminrezo/secboard && cd secboard`
- Copy .env.local to .env and set your variables
- Start build and run `./bin/start.sh`
- Then go to <https://your.domain.name>.
- Default credentials are : `admin@admin` / `admin@admin`


## Without Docker (tested on Debian)

- Install a DB engine (PostgreSQL ?), a mail server (Postfix ?), 
 a reverse proxy (Nginx ?) on your side.
- Clone the repo `git clone https://gitlab.com/adminrezo/secboard && cd secboard`
- `cp .env.local .env`
- Set your variables in .env
- Install the app :

    bundle install
    rm config/credentials.yml.enc
    ./bin/rails credentials:edit
    ./bin/rails assets:precompile
    ./bin/rails assets:clean
    ./bin/rails db:create
    ./bin/rails db:migrate
    ./bin/rails db:seed
    ./bin/rails s

- Then go to <http://127.0.0.1:3000>.
- Default credentials are : `admin@admin` / `admin@admin`

