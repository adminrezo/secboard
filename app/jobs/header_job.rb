# frozen_string_literal: true

class HeaderJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @dn = @domain.name
    headers
  end

  private

  def headers
    rsp = HTTParty.head("https://#{@dn}")
    rsp.headers.each_key do |key|
      @k = key
      @v = rsp.headers[key].downcase
      note
      Header.create(name: @k, value: @v,
                    note: @nt, domain: @domain)
    end
  end

  def note
    @nt = 6
    simple_headers
    test_sts      if @k == 'strict-transport-security'
    test_xcto     if @k == 'x-content-type-options'
    test_xfo      if @k == 'x-frame-options'
    test_xxss     if @k == 'x-xss-protection'
  end

  def simple_headers
    @nt = 4       if @k == 'server' || @k == 'via'
    @nt = 4       if @k.match('x-')
    @nt = 10      if (@k == 'content-security-policy') ||
                     (@k == 'feature-security') ||
                     (@k == 'permissions-policy')
  end

  def test_sts
    time = @v.split('=')[1]
    time = time.split(';')[0].to_i
    @nt = 10 if time >= 31_536_000
    @nt = 5 if (time < 31_536_000) || !@v.match('preload')
  end

  def test_xcto
    @nt = 0
    @nt = 10 if @v == 'nosniff'
  end

  def test_xfo
    @nt = 0
    @nt = 10      if (@v == 'deny') || (@v == 'sameorigin')
  end

  def test_xxss
    @nt = 0       if @k == 'x-xss-protection'
    @nt = 10      if (@k == 'x-xss-protection') && (@v == '1; mode=block')
  end
end
