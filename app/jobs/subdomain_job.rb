# frozen_string_literal: true

class SubdomainJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @md = maindomain(@domain.name)
    find_subs
    find_wildcard
    Subdomain.create(name: '-', note: 10, domain: @domain) if @domain.subdomains.empty?
  end

  def find_subs
    names = []
    rsp = HTTParty.get("https://crt.sh?q=#{@md}&output=json")
    parsed = JSON.parse(rsp.body)
    parsed.each { |entity| names.push(entity['common_name']) }
    names = names.uniq
    names.each { |name| Subdomain.create(name:, note: 7, domain: @domain) }
  end

  def find_wildcard
    r = Resolv::DNS.new
    addr = r.getresources("*.#{@md}", Resolv::DNS::Resource::IN::A).map(&:address)
    Subdomain.create(name: "*.#{@md}", note: 7, domain: @domain) unless addr.empty?
  end
end
