# frozen_string_literal: true

class DnsPropagationJob < ApplicationJob
  queue_as :default

  def initialize(fqdn)
    super
    @fqdn = fqdn
    @dnslist = { 'Online' => ['🇫🇷', 'France', '163.172.107.158'], 'OpenDNS' => ['🇺🇸', 'USA', '208.67.222.220'],
                 'Teknet Yazlim' => ['🇹🇷', 'Turkey', '31.7.37.37'], 'Pyton' => ['🇳🇱', 'Netherland', '193.58.204.59'],
                 'Railwire' => ['🇮🇳', 'India', '112.133.219.34'], 'Google' => ['🇺🇸', 'USA', '8.8.8.8'],
                 'KT Corp' => ['🇰🇷', 'South Corea', '168.126.63.1'], 'AT&T' => ['🇺🇸', 'USA', '12.121.117.201'],
                 'Cloudflare' => ['🇦🇺', 'Australia', '1.1.1.1'], 'ServiHosting' => ['🇪🇸', 'Spain', '84.236.142.130'],
                 'SkyDNS' => ['🇷🇺', 'Russia', '195.46.39.39'], 'Verizon' => ['🇬🇧', 'UK', '158.43.128.1'],
                 'IPP' => ['🇨🇳', 'China', '114.114.115.119'], 'Sprint' => ['🇺🇸', 'USA', '204.117.214.10'] }
  end

  def explore
    rsp = {}
    @dnslist.each do |server|
      response = resolve(server)
      rsp["#{server[1][0]} #{server[0]}"] = response
    end
    rsp
  end

  private

  def resolve(server)
    rsp = nil
    r = Resolv::DNS.new(nameserver: [server[1][2]])
    r.timeouts = 1
    addr = begin
      r.getresources(@fqdn, Resolv::DNS::Resource::IN::A).map(&:address)
    rescue StandardError
      'No result'
    end
    addr.each { |a| rsp = rsp.nil? ? a.to_s : "#{rsp}, #{a}" }
    rsp
  end
end
