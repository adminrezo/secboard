# frozen_string_literal: true

class JslibJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    http_data
    @data.each do |lib|
      library = lib['src'] if !lib['src'].nil? && lib['src'].match('/')
      next unless library

      version = library.match(/[0-9]+\.[0-9]+\.[0-9]+/).to_s
      note = notelib(library, version)
      library = library.split('?')[0]
      Jslib.create(library:, version:, note:, domain: @domain)
    end
  end

  private

  def http_data
    http = HTTParty.get("https://#{@domain.name}")
    @data = Nokogiri::HTML(http).xpath('//script')
  end

  def notelib(library, version)
    version ||= ''
    note = version == '' ? 7 : 4
    note = 0 if library.match('http://')
    note
  end
end
