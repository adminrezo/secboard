# frozen_string_literal: true

# require 'nmap/command' frozen_string_literal: true

class PortJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @filename = "tmp/k2/#{@domain.name}.ports"
    portscan
    xmlparse
    File.delete(@filename)
  end

  private

  def portscan
    require 'nmap/command'
    ports = [21, 22, 23, 25, 53, 80, 110, 111, 135, 139, 143, 443, 445, 993, 995, 1723, 3306, 3389, 5900, 8080]
    Nmap::Command.run do |nmap|
      nmap.connect_scan = true
      nmap.service_scan = true
      nmap.ports = ports
      nmap.targets = @domain.name
      nmap.output_xml = @filename
    end
  end

  def xmlparse
    require 'nmap/xml'
    Nmap::XML.open(@filename) do |xml|
      xml.each_host do |host|
        host.each_port do |port|
          next unless port.state == :open

          create_port(port)
        end
      end
    end
  end

  def create_port(port)
    note = (port.number == 80) || (port.number == 443) ? 7 : 0
    software = "#{port.service.name} #{port.service.product}"
    Port.create(
      number: port.number, protocol: port.protocol.to_s,
      software:, version: port.service.version,
      note:, domain: @domain
    )
  end
end
