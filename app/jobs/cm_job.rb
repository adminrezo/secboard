# frozen_string_literal: true

class CmJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @dn = domain.name
    @fn = "MODULES/cmseek/Result/#{@dn}/cms.json"
    cms
  end

  private

  def cms
    # cmd = "python3 MODULES/cmseek/cmseek.py --light-scan --batch -u #{@dn}"
    system('python3', 'MODULES/cmseek/cmseek.py',
           '--light-scan', '--batch', '-u', @dn)
    File.open @fn do |file|
      file.find do |line|
        next unless line.include?('cms_name') || line.include?('cms_version')

        @cmsname = line.split('"')[3] if line.include?('cms_name')
        @cmsvers = line.split('"')[3] if line.include?('cms_version')
      end
    end
    create_cms
  end

  def create_cms
    note = 10
    note = 5 if @cmsname.present?
    note = 3 if @cmsvers.present?
    if @cmsname.blank?
      (@cmsname = '-'
       @cmsvers = '-')
    end
    Cm.create(name: @cmsname, version: @cmsvers, note:, domain: @domain)
  end
end
