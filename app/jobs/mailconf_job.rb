# frozen_string_literal: true

class MailconfJob < ApplicationJob
  queue_as :default

  def perform(domain)
    @domain = domain
    @maindom = maindomain(domain.name)
    @dns = Resolv::DNS.new(nameserver: ['8.8.8.8', '8.8.4.4'])

    spf
    dmarc
  end

  private

  def spf
    resp = @dns.getresources @maindom, Resolv::DNS::Resource::IN::TXT
    resp.each do |r|
      txt = r.strings[0].downcase
      next unless txt.match('v=spf')

      note = note_spf(txt)
      Mailconf.create(protocol: 'SPF', value: txt, note:, domain: @domain)
    end
    ms = Mailconf.where(domain: @domain, protocol: 'SPF')
    Mailconf.create(protocol: 'SPF', value: nil, note: 0, domain: @domain) if ms.empty?
  end

  def dmarc
    resp = @dns.getresources "_dmarc.#{@maindom}", Resolv::DNS::Resource::IN::TXT
    resp.each do |r|
      txt = r.strings[0].downcase
      next unless txt.match('v=dmarc')

      policy = txt.split('p=')[1].split(';')[0]
      note = note_dmarc(policy)
      Mailconf.create(protocol: 'DMARC', value: txt, note:, domain: @domain)
    end
    ms = Mailconf.where(domain: @domain, protocol: 'DMARC')
    Mailconf.create(protocol: 'DMARC', value: nil, note: 0, domain: @domain) if ms.empty?
  end

  def note_spf(txt)
    case txt
    when /~all/
      5
    when /-all/
      10
    else
      2
    end
  end

  def note_dmarc(policy)
    case policy
    when 'quarantine'
      8
    when 'reject'
      10
    else
      5
    end
  end
end
