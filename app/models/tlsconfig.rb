# frozen_string_literal: true

class Tlsconfig < ApplicationRecord
  belongs_to :domain
end
