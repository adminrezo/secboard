# frozen_string_literal: true

class Cm < ApplicationRecord
  belongs_to :domain
end
