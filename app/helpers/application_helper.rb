# frozen_string_literal: true

module ApplicationHelper
  def back_to(uri)
    link_to t('shared_words.back'), uri
  end
end
