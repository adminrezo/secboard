# frozen_string_literal: true

module DomainsHelper
  def show_note_tags(note)
    case note
    when 0..3
      "<span class='tag is-danger'><i class='fa fa-warning' aria-hidden='true'></i></span>".html_safe
    when 4..5
      "<span class='tag is-warning'><i class='fa fa-warning' aria-hidden='true'></i></span>".html_safe
    when 6..8
      "<span class='tag is-info'><i class='fa fa-info' aria-hidden='true'></i></span>".html_safe
    when 9..10
      "<span class='tag is-success'><i class='fa fa-shield' aria-hidden='true'></i></span>".html_safe
    end
  end

  def show_note_pdf(note)
    case note
    when 0..3
      "<span class='tag is-danger'>✖ </span>".html_safe
    when 4..5
      "<span class='tag is-warning'>❗</span>".html_safe
    when 6..8
      "<span class='tag is-info'>𝐢</span>".html_safe
    when 9..10
      "<span class='tag is-success'>✔<span>".html_safe
    end
  end

  def show_empty(colspan)
    "<td colspan='#{colspan}' class='has-text-centered'>#{t('nothing')}</td>"
  end

  def show_title(title, name)
    content = "#{name} - #{title}"
    content_tag(:p, content, class: 'subtitle block')
  end

  def fa_icon(icon, clss, text)
    text = content_tag(:span, t(text), class: clss.to_s) unless text.empty?
    icon = content_tag(:span, content_tag(:i, '', class: "fa #{icon}"), class: "icon #{clss}")
    icon + text
  end
end
