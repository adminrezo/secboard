# frozen_string_literal: true

# Web Dig
class DigController < ApplicationController
  # GET /tools/dig
  def index; end

  # POST /tools/dig
  def show
    @fqdn = params[:fqdn]
    @result = {}
    @r = Resolv::DNS.new
    qry_a_aaaa
    qry_cname_ns
    qry_mx
    qry_srv
    qry_txt
  end

  private

  def qry_a_aaaa
    @result['A'] = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::A).map(&:address)
    @result['AAAA'] = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::AAAA).map(&:address)
  end

  def qry_cname_ns
    @result['CNAME'] = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::CNAME).map(&:name)
    @result['NS'] = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::NS).map(&:name)
  end

  def qry_mx
    @result['MX'] = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::MX).map(&:exchange)
  end

  def qry_srv
    a = []
    resources = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::SRV).map
    resources.each do |r|
      a << r.strings[0]
    end
    @result['SRV'] = a
  end

  def qry_txt
    a = []
    resources = @r.getresources(@fqdn, Resolv::DNS::Resource::IN::TXT).map
    resources.each do |r|
      a << r.strings[0]
    end
    @result['TXT'] = a
  end
end
