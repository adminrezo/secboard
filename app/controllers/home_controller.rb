# frozen_string_literal: true

# Home page only
class HomeController < ApplicationController
  # GET /
  def index
    @domain = Domain.new
  end
end
