# frozen_string_literal: true

# Encode/Decode Base64
class Base64Controller < ApplicationController
  # GET /tools/base64
  def index; end

  # POST /tools/base64/encode
  def show
    text = params[:b64txt]
    action = params[:code]
    begin
      @result = Base64.strict_encode64(text) if text && (action == 'en')
      @result = Base64.strict_decode64(text) if text && (action == 'de')
    rescue StandardError
      @result = t('.nothing')
    end
  end
end
