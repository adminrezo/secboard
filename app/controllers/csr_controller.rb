# frozen_string_literal: true

# Decode CSR fields
class CsrController < ApplicationController
  # GET /tools/csr
  def index; end

  # POST /tools/csr
  def show
    @csr = params[:csr]
    begin
      rqst = OpenSSL::X509::Request.new(@csr).subject
    rescue StandardError
      rqst = Hash('CSR' => t('.nothing'))
    end
    @csrhash = rqst.to_a
  end
end
