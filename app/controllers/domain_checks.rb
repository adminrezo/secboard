# frozen_string_literal: true

class DomainChecks
  def initialize(domain)
    @domain = domain
    @domain.name = Shellwords.shellescape(@domain.name)
  end

  def test_domain
    if domain_inbase?
      @notice = 'domainexist' if domain_inbase?
    elsif domain_exists?
      @notice = 'scanrunning' if @domain.save
    else
      @notice = 'domainnotvalid'
    end
    [@domain, @notice]
  end

  private

  # Takes a domain object - If domain is found domain become this existent domain - Returns true or false
  def domain_inbase?
    ds = Domain.find_by(name: @domain.name, user: @domain.user)
    return false if ds.blank?

    @domain = ds

    true
  end

  # Takes a domain name - Returns true or false
  def domain_exists?
    begin
      Addrinfo.getaddrinfo(@domain.name, nil)
    rescue SocketError
      return false
    end
    true
  end
end
