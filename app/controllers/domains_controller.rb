# frozen_string_literal: true

class DomainsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_domain, except: %i[index new create destroy_all]
  layout :define_layout

  # GET /domains or /domains.json
  def index
    @domains = Domain.where(user: current_user)
  end

  # GET /domains/1 or /domains/1.json
  def show
    respond_to do |format|
      format.html
      format.pdf { render_pdf }
    end
  end

  # GET /domains/headers/1
  def show_headers = @lyt = 'domains_table'

  # GET /domains/tls/1
  def show_tls = @lyt = 'domains_table'

  # GET /domains/jslibs/1
  def show_jslibs = @lyt = 'domains_table'

  # GET /domains/cms/1
  def show_cms = @lyt = 'domains_table'

  # GET /domains/mail/1
  def show_mail = @lyt = 'domains_table'

  # GET /domains/ports/1
  def show_ports = @lyt = 'domains_table'

  # GET /domains/subdomains/1
  def show_subdomains = @lyt = 'domains_table'

  # GET /domains/new
  def new = @domain = Domain.new

  # POST /domains or /domains.json
  def create
    @domain = Domain.new(domain_params)
    @domain.user = current_user
    result = DomainChecks.new(@domain).test_domain
    @domain = result[0]
    @notice = result[1]
    redirect_domain
  end

  # GET /domains/1/update
  def update
    domainrecreate
    dojobs
    redirect_to "/domains/#{@domain.id}", notice: t('domains.scanrunning') and return
  end

  # DELETE /domains/1 or /domains/1.json
  def destroy
    @domain.destroy
    respond_to do |format|
      format.html { redirect_to '/domains', notice: t('domains.domaindestroyed') }
    end
  end

  # GET /domains/delete
  def destroy_all
    ds = Domain.where(user: current_user)
    ds.each(&:destroy)
    respond_to do |format|
      format.html { redirect_to '/domains', notice: t('domains.domainsdestroyed') }
    end
  end

  private

  def redirect_domain
    if @notice == 'domainnotvalid'
      redirect_to '/', notice: t(@notice)
    else
      dojobs unless @notice == 'domainexist'
      redirect_to "/domains/#{@domain.id}", notice: t(@notice)
    end
  end

  def set_domain = @domain = Domain.find(params[:id])

  def domain_params = params.require(:domain).permit(:name, :scanresult)

  def domainrecreate
    @domain.name = Shellwords.shellescape(@domain.name)
    dn = @domain.name
    id = @domain.id
    @domain.destroy
    @domain = Domain.create(id:, name: dn, user: current_user)
  end

  def save_or_notice
    @notice = 'scanrunning'
    ds = Domain.where(name: @domain.name, user: @domain.user)
    if ds.empty?
      @notice = 'domainnotvalid' unless domain_exists?(@domain.name) && @domain.save
    else
      ds.each do |d|
        @domain = d
        @notice = 'domainexist'
      end
    end
  end

  def dojobs
    CmJob.perform_later @domain
    HeaderJob.perform_later @domain
    JslibJob.perform_later @domain
    MailconfJob.perform_later @domain
    PortJob.perform_later @domain
    SubdomainJob.perform_later @domain
    TlsconfigJob.perform_later @domain
  end

  def define_layout = @lyt ? 'domains_table' : 'domains'

  def render_pdf
    cover = "<h1 class='title'>K2</h1><h2 class='subtitle'>#{t('domains.title')} #{@domain.name}</h2>"
    cover += "<img src='/images/background-pdf.jpg' />"
    thedate1 = Time.zone.now.strftime('%Y-%m-%d_%H-%M-%S')
    thedate2 = Time.zone.now.strftime('%Y/%m/%d %H:%M')
    render pdf: "report_#{@domain.name}_#{thedate1}", layout: 'pdf.html.erb', encoding: 'utf8', cover:,
           margin: { top: 20, bottom: 20, left: 30, right: 30 },
           toc: { header_text: t('domains.toc') }, header: { left: "K2 - #{t('domains.subtitle')}" },
           footer: { right: "#{t('domains.show.update')} : #{thedate2}", left: @domain.name }
  end
end
